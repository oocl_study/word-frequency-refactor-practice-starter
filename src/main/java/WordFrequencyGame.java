import java.util.*;
import java.io.CharArrayWriter;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            String[] wordSplit = inputStr.split("\\s+");

            List<String> wordList = Arrays.asList(wordSplit);
            Map<String, Integer> wordCountMap = calculateWordCount(wordList);

            List<Input> inputList = toInputList(wordCountMap);

            inputList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());

            return formatInput(inputList);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private List<Input> toInputList(Map<String, Integer> wordCountMap) {
        return wordCountMap.keySet().stream().map(word -> new Input(word, wordCountMap.get(word))).collect(Collectors.toList());
    }

    private Map<String, Integer> calculateWordCount(List<String> wordList) {
        Map<String, Integer> wordCountMap = new HashMap<>();
        wordList.forEach(inputString -> {
            if (wordCountMap.containsKey(inputString)) {
                wordCountMap.put(inputString, wordCountMap.get(inputString) + 1);
            } else {
                wordCountMap.put(inputString, 1);
            }
        });
        return wordCountMap;
    }

    private String formatInput(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.stream().forEach(input -> joiner.add(input.getValue() + " " + input.getWordCount()));
        return joiner.toString();
    }
}
